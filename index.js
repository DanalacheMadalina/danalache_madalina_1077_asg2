
const FIRST_NAME = "Madalina";
const LAST_NAME = "Danalache";
const GRUPA = "1077";

/**
 * Make the implementation here
 */

function initCaching() {
  var cache = new Object();

  cache.pageAccessCounter=function(sectionWeb = 'home')
    {
    sectionWeb=sectionWeb.toLowerCase();
    if(cache.hasOwnProperty(sectionWeb))
    {
        cache[sectionWeb]++;
    }
    else
    {
        Object.defineProperty(cache,sectionWeb,{ value:1 , writable:true});
    }
  }
  cache.getCache = function()
  {
      return this;
  }
  return cache;   
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    initCaching
}

